## Fiche de risque

<!--
Le titre de l'issue est le libellé du risque
-->

## Détails
<!-- 
Expliquez: 
- les facteurs du risque (causes) 
- et ses conséquences (effets)
-->


## Actions éventuelles pour diminuer l'impact du risque

| Num  | Action            | Responsable(s)         | 
| ---- | ----------------- | ---------------------- | 
|      |                   |                        | 
|      |                   |                        | 
|      |                   |                        | 
|      |                   |                        | 


<!-- 
- Déterminez la nature du risque
- Déterminez l'impact/importance du risque
- Précisez la milestone si vous voulez gérer un notion de temps (le moment où cela doit être "réglé")
-->

/milestone %01-Qualification %02-Analysis %"03-Technical proposal" %"04-Financial proposal" %05-Submit
/label ~"RISK::FINANCIAL" ~"RISK::MANAGEMENT" ~"RISK::REQUIREMENT" ~"RISK::SECURITY" ~"RISK::OTHER" ~"IMPACT::LOW" ~"IMPACT::AVERAGE" ~"IMPACT::HIGH"


